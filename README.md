# ConsoleWriter

A textbox console writer library for C# winforms.

## Getting Started

Compling yourself: Use Visual Studio and the project file to create either a new project to be modified or add to a solution for use as a DLL.

### Requires:

This project is target for .NET framework 4.7.2 It should be compliant with earlier and later versions of course, however when compiling with VS, one will have to change the project settings.

### Installing

Use Visual Studio with the project as a reference to build the project file into an DLL for an app.

### Calling Redirection

Just set the output of the console to the new stream using:
```csharp
consoleRedirect = new TextBoxStreamWriter(textBox1);
Console.SetOut(consoleRedirect);
```

## Authors

* **Daniel Stamer-Squair** - *UaineTeine*

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details